<body>
    <form action="/welcome" method="post">
        @csrf
        <h1>Buat Account Baru!</h1>
        <h3>Sign Up Form</h3>
        <p>First name:</p>
        <input type="text" name="first">
        <br>
        <p>Last name:</p>
        <input type="text" name="last">
        <p>Gender:</p>
        <input type="radio" name="gender" value="male">Male
        <br>
        <input type="radio" name="gender" value="female">Female
        <p>Nationality:</p>
        <select>
            <option>Indonesian</option>
            <option>Amerika</option>
            <option>Inggris</option>
        </select>
        <p>Language Spoken:</p>
        <input type="checkbox">Bahasa Indonesia
        <br>
        <input type="checkbox">English
        <br>
        <input type="checkbox">Other
        <p>Bio:</p>
        <textarea cols="40" rows="10"></textarea>
        <br><br>
        <input type="submit" value="Sign Up">
    </form>
</body>
<br>
<br>
