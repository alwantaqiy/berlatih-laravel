<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function regis(){
        return view('register');
    }

    public function kirim(request $request){
        $nama1 = $request['first'];
        $nama2 = $request['last'];
        return view('welcome', compact('nama1', 'nama2'));       
    }
}
